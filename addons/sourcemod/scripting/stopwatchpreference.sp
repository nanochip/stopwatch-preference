#pragma semicolon 1

#define PLUGIN_AUTHOR "Nanochip"
#define PLUGIN_VERSION "1.0"

#include <sourcemod>
#include <clientprefs>
#include <tf2>
#include <sdkhooks>
#include <morecolors>
#include <gamemode>

public Plugin myinfo =
{
	name = "[TF2] Stopwatch Preference",
	author = PLUGIN_AUTHOR,
	description = "Determines wether stopwatch mode should be enabled/disabled based on player preference.",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/xNanochip/"
};

ConVar cvarPercent, cvarMinPlayers, cvarTournament;
Cookie cStopwatchPref;
bool g_bWaitingForPlayers;
bool g_bHasCalculated; //has the preferences already been calculated?

TF2_GameMode g_Gamemode;

int g_iStopwatchPref[MAXPLAYERS+1] = {-1, ...}; // -1 = Pref not set, 0 = Off, 1 = On

public void OnPluginStart()
{
	CreateConVar("stopwatchpreference_version", PLUGIN_VERSION, "Stopwatch Preference Version", FCVAR_DONTRECORD);
	
	cvarPercent = CreateConVar("stopwatchpreference_percentage", "0.60", "How many players who prefer stopwatch on compared to off would activate stopwatch? 0.60 = 60%.", 0, true, 0.05, true, 1.0);
	cvarMinPlayers = CreateConVar("stopwatchpreference_minplayers", "3", "The minimum number of players required to have stopwatch mode preferred to be on in order for it to activate regardless of percentage.");
	cvarTournament = FindConVar("mp_tournament");
	
	RegConsoleCmd("sm_stopwatch", Cmd_Stopwatch, "Change your stopwatch preference");
	RegAdminCmd("sm_forcestopwatch", Cmd_ForceStopwatch, ADMFLAG_RCON, "Force toggle the status of stopwatch");
	
	//are these even real client commands?
	AddCommandListener(BlockCommand, "tournament_player_readystate");
	AddCommandListener(BlockCommand, "tournament_readystate");
	AddCommandListener(BlockCommand, "tournament_teamname");
	
	cStopwatchPref = new Cookie("stopwatch_preference", "Preference for Stopwatch Mode", CookieAccess_Private);
	HookEvent("player_spawn", OnPlayerSpawn);
	
	AutoExecConfig();
	
	for (int i = 1; i <= MaxClients; i++)
	{
		g_iStopwatchPref[i] = -1;
		if (AreClientCookiesCached(i)) OnClientCookiesCached(i); //late load
	}
}

public void OnClientConnected(int client)
{
	g_iStopwatchPref[client] = -1;
}

public void OnClientCookiesCached(int client)
{
	char info[16];
	cStopwatchPref.Get(client, info, sizeof(info));
	if (strcmp(info, "on") == 0) g_iStopwatchPref[client] = 1;
	if (strcmp(info, "off") == 0) g_iStopwatchPref[client] = 0;
}

public void OnMapStart()
{
	g_Gamemode = TF2_DetectGameMode();
	g_bWaitingForPlayers = false;
	g_bHasCalculated = false;
}

public void OnMapEnd() //TODO this is not working, need to figure out how to disable it before the map is about to change
{
	if (cvarTournament.BoolValue) cvarTournament.SetBool(false);
}

public void OnEntityCreated(int entity, const char[] classname)
{
	if (strcmp(classname, "team_round_timer") == 0)
    {
    	if (g_bWaitingForPlayers) return;
    	if (g_Gamemode != TF2_GameMode_PL && g_Gamemode != TF2_GameMode_ADCP) return; //only allow stopwatch on PL and A/D
        if (!g_bHasCalculated) HookSingleEntityOutput(entity, "On30SecRemain", OnEntityOutput, false);
    }
}

public void OnEntityOutput(const char[] output, int caller, int activator, float delay)
{
	if (strcmp(output, "On30SecRemain") == 0)
	{
		int on, off;
		float percent, limit;
		for (int i = 1; i <= MaxClients; i++)
		{
			if (g_iStopwatchPref[i] == 1) on++;
			if (g_iStopwatchPref[i] == 0) off++;
		}
		
		percent = float(on) / float(off);
		limit = cvarPercent.FloatValue;
		
		if (on >= cvarMinPlayers.IntValue)
		{
			if (FloatCompare(percent, limit) >= 0)
			{
				EnableStopwatch();
				CPrintToChatAll("[{green}SM{default}] Stopwatch mode has been {green}enabled{default}. {lightgreen}%f%% {default}of players prefer it on.", percent);
			}
			else
			{
				CPrintToChatAll("[{green}SM{default}] Keeping Stopwatch mode off. {lightgreen}%f%% {default}of players prefer it off.", 100-percent);
			}
		}
		else
		{
			CPrintToChatAll("[{green}SM{default}] Keeping Stopwatch mode off. There must be a minimum of {lightgreen}%d {default}\"On\" preferences (Only received {lightgreen}%d{default}).", cvarMinPlayers.IntValue, on);
		}
		
		g_bHasCalculated = true;
	}
}

public Action OnPlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	if (g_Gamemode != TF2_GameMode_PL && g_Gamemode != TF2_GameMode_ADCP) return Plugin_Continue; //only show stopwatch pref menu on PL and A/D
	if (g_bHasCalculated) return Plugin_Continue;
	
	int client = GetClientOfUserId(event.GetInt("userid"));
	if (g_iStopwatchPref[client] == -1 && !IsFakeClient(client))
	{
		StopwatchPrefMenu(client);
	}
	return Plugin_Continue;
}

public Action Cmd_ForceStopwatch(int client, int args)
{
	if (!GameRules_GetProp("m_bInSetup") && g_bHasCalculated) //the lazy way of allowing this command...
	{
		CReplyToCommand(client, "[{green}SM{default}] You may only run this command during setup round after Stopwatch preferences have already been calculated.");
		return Plugin_Handled;
	}
	if (cvarTournament.BoolValue)
	{
		DisableStopwatch();
		CPrintToChatAllEx(client, "[{green}SM{default}] {teamcolor}%N {default}forcefully {red}disabled {default}Stopwatch mode.", client);
	}
	else
	{
		EnableStopwatch();
		CPrintToChatAllEx(client, "[{green}SM{default}] {teamcolor}%N {default}forcefully {green}enabled {default}Stopwatch mode.", client);
	}
	return Plugin_Handled;
}

public void OnClientPutInServer(int client)
{
	CreateTimer(5.0, StopwatchMessage, GetClientUserId(client), TIMER_FLAG_NO_MAPCHANGE);
}

public Action StopwatchMessage(Handle hTimer, int userid)
{
	int client = GetClientOfUserId(userid);
	if (client == 0) return Plugin_Continue;

	if (cvarTournament.BoolValue)
	{
		CPrintToChat(client, "[{green}SM{default}] Stopwatch mode is currently {green}enabled{default}.");
		if (g_iStopwatchPref[client] == -1)
		{
			CPrintToChat(client, "[{green}SM{default}] Set your Stopwatch mode preference by typing {lightgreen}!stopwatch");
		}
	}
	return Plugin_Continue;
}

public Action BlockCommand(int client, const char[] command, int argc)
{
	if (cvarTournament.BoolValue) return Plugin_Stop;
	return Plugin_Continue;
}

public Action Cmd_Stopwatch(int client, int args)
{
	if (client > 0)
	{
		StopwatchPrefMenu(client);
	}
	return Plugin_Handled;
}

void StopwatchPrefMenu(int client)
{
	Menu menu = new Menu(StopwatchPrefMenuHandler);
	menu.SetTitle("Do you prefer Stopwatch mode ON or OFF?");
	menu.AddItem("on", "On");
	menu.AddItem("off", "Off");
	menu.ExitButton = false;
	menu.Display(client, 20);
}

public int StopwatchPrefMenuHandler(Menu menu, MenuAction action, int client, int param2)
{
	switch(action)
	{
		case MenuAction_Select:
		{
			char info[16];
			menu.GetItem(param2, info, sizeof(info));
			if (strcmp(info, "on") == 0)
			{
				CPrintToChat(client, "[{green}SM{default}] You have set your Stopwatch mode preference to: {green}On");
				g_iStopwatchPref[client] = 1;
				cStopwatchPref.Set(client, "on");
			}
			if (strcmp(info, "off") == 0)
			{
				CPrintToChat(client, "[{green}SM{default}] You have set your Stopwatch mode preference to: {red}Off");
				g_iStopwatchPref[client] = 0;
				cStopwatchPref.Set(client, "off");
			}
			if (g_bHasCalculated)
			{
				CPrintToChat(client, "[{green}SM{default}] Stopwatch mode has already been calculated but your preference has been saved and will apply to the next Stopwatch mode calculation.");
			}
		}
		case MenuAction_End: delete menu;
	}
}

void EnableStopwatch()
{
	ServerCommand("mp_tournament 1");
	ServerCommand("mp_tournament_stopwatch 1");
}

void DisableStopwatch()
{
	ServerCommand("mp_tournament 0");
	ServerCommand("mp_tournament_stopwatch 0");
}

public void TF2_OnWaitingForPlayersStart()
{
	g_bWaitingForPlayers = true;
}

public void TF2_OnWaitingForPlayersEnd()
{
	g_bWaitingForPlayers = false;
}